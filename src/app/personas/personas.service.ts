import { Injectable } from '@angular/core';
import { Persona } from './persona';

@Injectable({
  providedIn: 'root'
})
export class PersonasService {

  private listaPersonas: Persona[] = [];

  constructor() { }

  guardarPersona(persona: Persona) {
    this.listaPersonas.push(persona);
  }

  getPersonas(): Persona[] {
    return this.listaPersonas;
  }
}
