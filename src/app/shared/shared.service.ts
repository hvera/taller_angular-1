import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Pais } from './pais';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  constructor(private http: HttpClient) { }

  getAllCountries(): Observable<Pais[]> {
    return this.http.get<Pais[]>('https://restcountries.eu/rest/v2/all');
  }
}
